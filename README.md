![](images/cs-logo.PNG)


# Coherent Smart Parking

## Introduction
Coherent Smart Parking application demonstrates Hyperledger Fabric deployment for a simple multi-organization private blockchain network. The application realizes the following fictitious use case.

## Use Case 
### Description
An organization owns an office building which has an adjacent parking lot. Employees of multiple business renting offices in the building use this parking lot. However, as there are not enough parking spaces 
spaces to everyone, the parking lot is crowded, people are frequently double and triple parking. This situation creates unnecessary frustration for tenants. The landlord decided to improve the way parking lot 
is used, bring more order, and thereby increase tenants' satisfaction with the property. A tracking solution will monitor parking lot occupancy, track parked cars that may be blocking other parking lot users 
and provide the ability to quickly notify these "blockers" to allow blocked people leave in a timely manner. As a reward for using the system, the landlord allocated a certain number of convenient 
parking spaces reserved for those consistently demonstrating good behavior.

### Trust
To avoid conflict and ensure mutual trust between the landlord and tenants, it was decided to use a transparent, blockchain-based solution. With that approach no single organization controls access to the
parking lot records and cannot provide preferential treatment to its own employees. Everyone in the system has a copy to the full historical and up to date parking lot records and the underlying technology
ensures that copies are identical all of the time. In the future, through advanced analytics it will let the organizations better understand employee behaviors, optimize parking lot usage and provide other
valuable features.

## Implementation
Smart Parking application is based on Blockchain (Hyperledger Fabric) and IoT, runs on Amazon Web Services leveraging IoT Core, Lambda, EC2, and simulates edge devices (cars).

![](https://hyperledger-fabric.readthedocs.io/en/latest/_images/hyperledger_fabric_logo_color.png)

### Business Structure Diagram
The following diagram demonstrates basic business architecture for the solution:

![](images/business-structure-diagram.png)

### Architecture diagram
Main components of the solution are depicted on the following diagram. Please note that the diagram illustrates the larger Smart Parking solution while this repository focuses on blockchain portion of it and at this time may not include everything depicted below.

![](images/architecture-diagram.png)

## Install 
    See ./SP/README.md
