#!/bin/sh

# Update system
sudo yum update -y

# Install additional soft
sudo yum install -y curl git mc

# Install Docker
sudo yum install -y docker
sudo service docker start
sudo usermod -a -G docker ec2-user

# Install Docker Compose
sudo curl -L \
https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname \
-s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod 0755 /usr/local/bin/docker-compose

# Install golang
curl -L https://dl.google.com/go/go1.11.11.linux-amd64.tar.gz -o go1.11.11.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.11.11.linux-amd64.tar.gz
rm -f go1.11.11.linux-amd64.tar.gz

# Install Node.js
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
source $HOME/.nvm/nvm.sh
nvm install 8.16.0

echo 'export PATH=/usr/local/go/bin:$PATH' >> $HOME/.bash_profile
echo 'export GOPATH=$HOME/go' >> $HOME/.bash_profile

echo 'export PATH=$HOME/fabric-samples/bin:$PATH' >> $HOME/.bash_profile
echo 'export PATH=$HOME/efs/repo/fabric-samples/bin:$PATH' >> $HOME/.bash_profile

source ~/.bash_profile 