# Local variables
locals {    
    app_dir                          = "$HOME/app"
    app_sec_dir                      = "${local.app_dir}/sec"
    app_sec_ssh_key_file             = "${local.app_sec_dir}/key"
    app_sec_swarm_token_file         = "${local.app_sec_dir}/token"
    app_fabric_dir                   = "${local.app_dir}/fabric"
    app_fabric_scripts_dir           = "${local.app_fabric_dir}/scripts"

    app_fabric_channel_artifacts_dir = "channel-artifacts"
    app_fabric_crypto_config_dir     = "crypto-config"
    app_fabric_artifacts_arch        = "artifacts.tar"
    app_fabric_artifacts_arch_w_path = "${local.app_fabric_dir}/${local.app_fabric_artifacts_arch}"
}

# Create a key pair
resource "aws_key_pair" "ec2key" {
    key_name                     = "smart-parking-key"
    public_key                   = "${file(var.public_key_path)}"
}

# Create EC2 instances

## Choose AMI
data "aws_ami" "amazon" {
    most_recent = true
    owners      = ["amazon"]

    filter {
        name    = "name"
        values  = ["amzn2-ami-hvm*"]
    }
  
    filter {
        name    = "architecture"
        values  = ["x86_64"]
    }

    filter {
        name    = "root-device-type"
        values  = ["ebs"]
    }

    filter {
        name    = "virtualization-type"
        values  = ["hvm"]
    }
}

# Create EC2 instance - Org 1
resource "aws_instance" "org1" {
    ami                    = data.aws_ami.amazon.id
    instance_type          = var.instance_type
    subnet_id              = var.vpc_public_subnet_id
    vpc_security_group_ids = var.vpc_security_group_ids
    key_name               = aws_key_pair.ec2key.key_name
    source_dest_check      = true

    root_block_device {
        volume_type = var.volume_type
        volume_size = var.volume_size
    }

    connection {
        type                = "ssh"
        host                = self.public_ip
        port                = 22
        user                = "ec2-user"
        password            = ""
        private_key         = "${file(var.private_key_path)}"
        timeout             = "60s"
    }

    provisioner "remote-exec" {
        inline = [
            "sudo hostnamectl set-hostname org1",
        ]
    }

    provisioner "remote-exec" {
        scripts = [
            "../scripts/init.sh"
        ]
    }

    provisioner "remote-exec" {
        inline = [
            "mkdir ${local.app_dir}",
            "mkdir ${local.app_sec_dir}",
            "docker swarm init --advertise-addr ${self.private_ip}",
            "docker network create --attachable --driver overlay spln-overlay",
            "docker swarm join-token --quiet worker | sudo tee ${local.app_sec_swarm_token_file}",
            "curl https://raw.githubusercontent.com/hyperledger/fabric/master/scripts/bootstrap.sh -o $HOME/bootstrap.sh",
            "chmod 700 $HOME/bootstrap.sh",
            "$HOME/bootstrap.sh 1.4.2 -d -s"
        ]
    }

    provisioner "file" {
        source      = "../app/"
        destination = "${local.app_dir}"
    }

    provisioner "remote-exec" {
        inline = [
            "cd ${local.app_fabric_scripts_dir}",
            "chmod 700 ./chain.sh",
            "cd ${local.app_fabric_dir}",
            "chmod 700 ./crypto.sh ./docker.sh ./deploy.sh",
            "./crypto.sh",
            "tar cfv ${local.app_fabric_artifacts_arch} ${local.app_fabric_channel_artifacts_dir}/ ${local.app_fabric_crypto_config_dir}/"
        ]
    }

    tags = {
        Name        = "${var.tag_name}-org1"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

# Create EC2 instance - Org X 
resource "aws_instance" "orgx" {

    count                  = var.slave_count

    ami                    = data.aws_ami.amazon.id
    instance_type          = var.instance_type
    subnet_id              = var.vpc_public_subnet_id
    vpc_security_group_ids = var.vpc_security_group_ids
    key_name               = aws_key_pair.ec2key.key_name
    source_dest_check      = true

    root_block_device {
        volume_type = "gp2"
        volume_size = "10"
    }

    connection {
        type                = "ssh"
        host                = self.public_ip
        port                = 22
        user                = "ec2-user"
        password            = ""
        private_key         = "${file(var.private_key_path)}"
        timeout             = "60s"
    }

    provisioner "remote-exec" {
        inline = [
            "sudo hostnamectl set-hostname org${count.index + 2}",
            "mkdir ${local.app_dir}",
            "mkdir ${local.app_sec_dir}",
            "echo '${file(var.private_key_path)}' >> ${local.app_sec_ssh_key_file}",
            "chmod 400 ${local.app_sec_ssh_key_file}"
        ]
    }

    provisioner "remote-exec" {
        scripts = [
            "../scripts/init.sh"
        ]
    }

    provisioner "file" {
        source      = "../app/"
        destination = local.app_dir
    }

    provisioner "remote-exec" {
        inline = [
            "scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i ${local.app_sec_ssh_key_file} ec2-user@${aws_instance.org1.private_ip}:${local.app_sec_swarm_token_file} ${local.app_sec_dir}",
            "sudo docker swarm join --token $(cat ${local.app_sec_swarm_token_file}) ${aws_instance.org1.private_ip}:2377",
            "cd ${local.app_fabric_dir}",
            "scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i ${local.app_sec_ssh_key_file} ec2-user@${aws_instance.org1.private_ip}:${local.app_fabric_artifacts_arch_w_path} ${local.app_fabric_dir}",
            "tar xfv ${local.app_fabric_artifacts_arch}"
        ]
    }

    tags = {
        Name        = "${var.tag_name}-org${count.index + 2}"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

# Provision - org1
resource "null_resource" "org1" {

    depends_on = [
        aws_instance.orgx
    ]

    connection {
        type                = "ssh"
        host                = aws_instance.org1.public_ip
        port                = 22
        user                = "ec2-user"
        password            = ""
        private_key         = "${file(var.private_key_path)}"
        timeout             = "60s"
    }

    provisioner "remote-exec" {
        inline = [
            "cd ${local.app_fabric_dir}",
            "./docker.sh",
            "./deploy.sh"
        ]
    }
}