output "org1_public_ip" {
     value     = "${aws_instance.org1.public_ip}"
     sensitive = true
}
