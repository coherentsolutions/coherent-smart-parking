# EC2
variable "public_key_path" {}
variable "private_key_path" {}
variable "instance_type" {}
variable "volume_type" {}
variable "volume_size" {}
variable "vpc_public_subnet_id" {}
variable "vpc_security_group_ids" {}
variable "slave_count" {}

# TAGs
variable "tag_name" {}
variable "tag_owner" {}
variable "tag_environment" {}