output "public_subnet_id" {
     value     = "${aws_subnet.public_subnet.id}"
     sensitive = true
}

output "security_group_ssh_sg_id" {
     value     = "${aws_security_group.ssh-sg.id}"
     sensitive = true
}

output "security_group_web_sg_id" {
     value     = "${aws_security_group.web-sg.id}"
     sensitive = true
}

output "security_group_swarm_sg_id" {
     value     = "${aws_security_group.swarm-sg.id}"
     sensitive = true
}

output "security_group_fabric_sg_id" {
     value     = "${aws_security_group.fabric-sg.id}"
     sensitive = true
}

output "vpc_id" {
     value = "${aws_vpc.vpc.id}"
     sensitive = true
}