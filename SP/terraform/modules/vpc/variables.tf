# VPC
variable "vpc_cidr" {
  description = "CIDR for the VPC"
}

variable "cidr_public_subnet" {
  description = "CIDR block for the subnet"
}

variable "cidr_private_subnet" {
  description = "CIDR block for the subnet"
}

# TAGs
variable "tag_name" {
  description = "tag - name"
}

variable "tag_owner" {
  description = "tag - owner"
}

variable "tag_environment" {
  description = "tag - environment"
}