## Declare the data source
data "aws_availability_zones" "available" {}

## Define VPC
resource "aws_vpc" "vpc" {
    cidr_block           = var.vpc_cidr
    enable_dns_hostnames = true

    tags = {
        Name        = "${var.tag_name}-vpc"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

## Define the public subnet
resource "aws_subnet" "public_subnet" {
    vpc_id                  = aws_vpc.vpc.id
    cidr_block              = var.cidr_public_subnet
    map_public_ip_on_launch = "true"
    availability_zone       = "${data.aws_availability_zones.available.names[0]}"

    tags = {
        Name        = "${var.tag_name}-public-sn"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

## Define the internet gateway
resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.vpc.id

    tags = {
        Name        = "${var.tag_name}-igw"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

## Define the route table
resource "aws_route_table" "rtb" {
    vpc_id = aws_vpc.vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }

    tags = {
        Name        = "${var.tag_name}-rt"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

### Assign the route table to the public Subnet
resource "aws_route_table_association" "rta" {
    subnet_id      = aws_subnet.public_subnet.id
    route_table_id = aws_route_table.rtb.id
}

## Define the security group for public subnet
resource "aws_security_group" "ssh-sg" {
    name = "ssh-sg"
    vpc_id = aws_vpc.vpc.id
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name        = "${var.tag_name}-ssh-sg"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

## Define the security group for public subnet
resource "aws_security_group" "web-sg" {
    name = "web-sg"
    vpc_id = aws_vpc.vpc.id
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name        = "${var.tag_name}-web-sg"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

## Define the security group for public subnet
resource "aws_security_group" "swarm-sg" {
    name = "swarm-sg"
    vpc_id = aws_vpc.vpc.id
#    ingress {
#        from_port   = "All"
#        to_port     = "All"
#        protocol    = "50"
#        cidr_blocks = ["0.0.0.0/0"]
#    }
    ingress {
        from_port   = 2377
        to_port     = 2377
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 4789
        to_port     = 4789
        protocol    = "udp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7946
        to_port     = 7946
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7946
        to_port     = 7946
        protocol    = "udp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name        = "${var.tag_name}-swarm-sg"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}

resource "aws_security_group" "fabric-sg" {
    name = "fabric-sg"
    vpc_id = aws_vpc.vpc.id
    ingress {
        from_port   = 5984
        to_port     = 5984
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 6060
        to_port     = 6060
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7050
        to_port     = 7050
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7051
        to_port     = 7051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7052
        to_port     = 7052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7053
        to_port     = 7053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 7054
        to_port     = 7054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8050
        to_port     = 8050
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8051
        to_port     = 8051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8052
        to_port     = 8052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8053
        to_port     = 8053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 8054
        to_port     = 8054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 9050
        to_port     = 9050
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 9051
        to_port     = 9051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 9052
        to_port     = 9052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 9053
        to_port     = 9053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 9054
        to_port     = 9054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 10050
        to_port     = 10050
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 10051
        to_port     = 10051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 10052
        to_port     = 10052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 10053
        to_port     = 10053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 10054
        to_port     = 10054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 11050
        to_port     = 11050
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 11051
        to_port     = 11051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 11052
        to_port     = 11052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 11053
        to_port     = 11053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 11054
        to_port     = 11054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 12051
        to_port     = 12051
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 12052
        to_port     = 12052
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 12053
        to_port     = 12053
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 12054
        to_port     = 12054
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name        = "${var.tag_name}-fabric-sg"
        Owner       = var.tag_owner
        Environment = var.tag_environment
    }
}
