# AWS
aws_region = "us-east-1"

# VPC
vpc_cidr = "10.0.0.0/16"

cidr_public_subnet = "10.0.16.0/24"

cidr_private_subnet = "10.0.32.0/24"

# EC2
public_key_path = "../keys/smart-parking-key.pub"

private_key_path = "../keys/smart-parking-key"

instance_type = "t3.small"

volume_type = "gp2"

volume_size = "20"

slave_count = 2

# TAGs
tag_name = "smart-parking"

tag_owner = "webinforequest@coherentsolutions.com"

tag_environment = "RnD"
