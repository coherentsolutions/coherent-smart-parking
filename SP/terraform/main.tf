# Terraform version
terraform {
    required_version = ">0.12"
}

# AWS
provider "aws" {
    version                      = "~> 2.19"
    region                       = var.aws_region
}

# Create VPC
module "vpc" {
    source = "./modules/vpc"

    vpc_cidr                     = var.vpc_cidr
    cidr_public_subnet           = var.cidr_public_subnet
    cidr_private_subnet          = var.cidr_private_subnet

    tag_name                     = var.tag_name
    tag_owner                    = var.tag_owner
    tag_environment              = var.tag_environment
}

# Create EC2
module "ec2" {
    source = "./modules/ec2"

    public_key_path              = var.public_key_path
    private_key_path             = var.private_key_path
    instance_type                = var.instance_type
    volume_type                  = var.volume_type
    volume_size                  = var.volume_size
    vpc_public_subnet_id         = module.vpc.public_subnet_id
    vpc_security_group_ids       = [
                                    module.vpc.security_group_ssh_sg_id,
                                    module.vpc.security_group_web_sg_id,
                                    module.vpc.security_group_swarm_sg_id,
                                    module.vpc.security_group_fabric_sg_id
    ]
    slave_count                   = var.slave_count

    tag_name                      = var.tag_name
    tag_owner                     = var.tag_owner
    tag_environment               = var.tag_environment
}
