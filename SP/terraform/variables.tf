# AWS
variable "aws_region" {
  description = "Region for the VPC"
}
# VPC
variable "vpc_cidr" {
  description = "CIDR for the VPC"
}
variable "cidr_public_subnet" {
  description = "CIDR block for the subnet"
}
variable "cidr_private_subnet" {
  description = "CIDR block for the subnet"
}
# EC2
variable "public_key_path" {
  description = "Public key path"
}
variable "private_key_path" {
  description = "Private key path"
}
variable "instance_type" {
  description = "type for aws EC2 instance"
}
variable "volume_type" {
  description = "type of volume"
}
variable "volume_size" {
  description = "size of volume"
}

variable "slave_count" {
  description = "count of slave nodes"
}
# TAGs
variable "tag_name" {
  description = "tag - name"
}
variable "tag_owner" {
  description = "tag - owner"
}
variable "tag_environment" {
  description = "tag - environment"
}