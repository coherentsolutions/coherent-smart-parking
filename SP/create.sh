#!/bin/sh

# SSH keys generation
if [ ! -f ./keys/smart-parking-key ] && [ ! -f ./keys/smart-parking-key.pub ]; then
   ssh-keygen -b 2048 -t rsa -f ./keys/smart-parking-key -q -N ""
fi

# Terraform
cd ./terraform

terraform init -input=false -reconfigure
terraform apply -input=false -lock=false -auto-approve

terraform output org1_public_ip