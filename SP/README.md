# Install Coherent Smart Parking

## Requirements
Install [HashiCorp Terraform](https://www.terraform.io/downloads.html) v0.12+

## Versions
- hosts OS - Amazon Linux 2
- hyperledger Fabric - v.1.4.2
- node.js - v.8.16
- go - v.1.11.11

## Variables
You can change variables in terraform.tfvars file.

    See ./terraform/terraform.tfvars

## Create
Create Smart Parking Hyperledger Fabric with create.sh script.

    Run ./create.sh

## Keys
SSH keys are generated in the first run of the create.sh script.
For using your own SSH keys:

- comment the strings which generate SSH keys in the create.sh script.
- copy your SSH keys to the ./keys folder.
- set it in the terraform.tfvars file.

## View
To view containers in the nodes - use a visualizer for Docker Swarm Mode:

    <org1_public_ip_address>:8080

## Destroy
Destroy Smart Parking Hyperledger Fabric with destroy.sh script.

    Run ./destroy.sh
