#!/bin/sh

# Terraform
cd ./terraform

terraform destroy -input=false -auto-approve -lock=false
