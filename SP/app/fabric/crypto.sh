#!/bin/sh

function clearContainers() {
  CONTAINER_IDS=$(docker ps -aq --filter name='dev-peer*')
  if [ -z "$CONTAINER_IDS" -o "$CONTAINER_IDS" == " " ]; then
    echo "---- No containers available for deletion ----"
  else
    docker rm -f $CONTAINER_IDS
  fi
}

function clearImages() {
  DOCKER_IMAGE_IDS=$(docker images --filter reference='dev-peer*' -q)
  if [ -z "$DOCKER_IMAGE_IDS" -o "$DOCKER_IMAGE_IDS" == " " ]; then
    echo "---- No images available for deletion ----"
  else
    docker rmi -f $DOCKER_IMAGE_IDS
  fi
}

function clearFolders() {
  if [ -d "crypto-config" ]; then
    rm -rf crypto-config
  fi

  if [ -d "channel-artifacts" ]; then
    rm -rf channel-artifacts
  fi
}

clearContainers
clearImages
clearFolders

mkdir channel-artifacts
mkdir crypto-config

# create the orderer genesis block
echo "#############################################################"
echo "##       Generate certificates using cryptogen tool        ##"
echo "#############################################################"
../../bin/cryptogen generate --config=./crypto-config.yaml
res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate certificates..."
  exit 1
fi

export FABRIC_CFG_PATH=$PWD

echo "#############################################################"
echo "##            Generating Orderer Genesis block             ##"
echo "#############################################################"
../../bin/configtxgen -profile ThreeOrgsOrdererGenesis -channelID spn-sys-channel -outputBlock ./channel-artifacts/genesis.block
res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi

# create the channel transaction artifact
export CHANNEL_NAME=spchannel

echo "#############################################################"
echo "##       Generating channel configuration transaction      ##"
echo "#############################################################"
../../bin/configtxgen -profile ThreeOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi

# define the anchor peer
echo "#############################################################"
echo "##         Generating anchor peer update for Org1MSP       ##"
echo "#############################################################"
../../bin/configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP

res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org1MSP..."
  exit 1
fi

echo "#############################################################"
echo "##         Generating anchor peer update for Org2MSP       ##"
echo "#############################################################"
../../bin/configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org2MSP..."
  exit 1
fi

echo "#############################################################"
echo "##        Generating anchor peer update for Org3MSP        ##"
echo "#############################################################"
../../bin/configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org3MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org3MSP

res=$?
if [ $res -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org2MSP..."
  exit 1
fi
