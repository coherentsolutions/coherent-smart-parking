#!/bin/sh
function replacePrivateKey() {
  
  CURRENT_DIR=$PWD
  
  cd crypto-config/peerOrganizations/org1.sp.com/ca/
  PRIV_KEY=$(ls *_sk)
  cd "$CURRENT_DIR"

  sed -i "s/CA1_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-stack1.yaml
  
  cd crypto-config/peerOrganizations/org2.sp.com/ca/
  PRIV_KEY=$(ls *_sk)
  cd "$CURRENT_DIR"

  sed -i "s/CA2_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-stack2.yaml
  
  cd crypto-config/peerOrganizations/org3.sp.com/ca/
  PRIV_KEY=$(ls *_sk)
  cd "$CURRENT_DIR"

  sed -i "s/CA3_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-stack3.yaml
}

echo "#############################################################"
echo "##                         DOCKER                          ##"
echo "#############################################################"

replacePrivateKey

export COMPOSE_PROJECT_NAME=net
export IMAGE_TAG=latest

docker stack deploy -c docker-compose-stack1.yaml -c docker-compose-stack2.yaml -c docker-compose-stack3.yaml sp
echo "================================"
echo "          40 sec delay          "
echo "================================"
sleep 40

echo "================================"
echo "            Services            "
echo "================================"
docker stack services sp

echo "================================"
echo "           Containers           "
echo "================================"
docker ps -a
