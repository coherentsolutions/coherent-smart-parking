#!/bin/sh

export CHANNEL_NAME=spchannel

echo "#############################################################"
echo "##                    Create channel                       ##"
echo "#############################################################"

peer channel create -o orderer.sp.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem

# Globals
CORE_PEER_MSPCONFIGPATH=/opt
CORE_PEER_ADDRESS=peer0
CORE_PEER_LOCALMSPID=""
CORE_PEER_TLS_ROOTCERT_FILE=/opt

setGlobals() {
    ORG=$1
    if [ $ORG -eq 1 ]; then
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.sp.com/users/Admin@org1.sp.com/msp
        CORE_PEER_ADDRESS=peer0.org1.sp.com:7051
        CORE_PEER_LOCALMSPID="Org1MSP"
        CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.sp.com/peers/peer0.org1.sp.com/tls/ca.crt
    elif [ $ORG -eq 2 ]; then
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.sp.com/users/Admin@org2.sp.com/msp
        CORE_PEER_ADDRESS=peer0.org2.sp.com:9051
        CORE_PEER_LOCALMSPID="Org2MSP"
        CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.sp.com/peers/peer0.org2.sp.com/tls/ca.crt
    elif [ $ORG -eq 3 ]; then
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.sp.com/users/Admin@org3.sp.com/msp
        CORE_PEER_ADDRESS=peer0.org3.sp.com:11051
        CORE_PEER_LOCALMSPID="Org3MSP"
        CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.sp.com/peers/peer0.org3.sp.com/tls/ca.crt
    else
        echo "================== ERROR !!! ORG Unknown =================="
    fi
}

echo "#############################################################"
echo "##                   Org1 channel join                     ##"
echo "#############################################################"

setGlobals 1
peer channel join -b $CHANNEL_NAME.block
sleep 4

echo "#############################################################"
echo "##                   Org2 channel join                     ##"
echo "#############################################################"

setGlobals 2
peer channel join -b $CHANNEL_NAME.block
sleep 4

echo "#############################################################"
echo "##                    Org3 channel join                    ##"
echo "#############################################################"

setGlobals 3
peer channel join -b $CHANNEL_NAME.block
sleep 4

echo "#############################################################"
echo "##                    Org1 channel update                  ##"
echo "#############################################################"

setGlobals 1
peer channel update -o orderer.sp.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem
sleep 4

echo "#############################################################"
echo "##                    Org2 channel update                  ##"
echo "#############################################################"

setGlobals 2
peer channel update -o orderer.sp.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org2MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem
sleep 4

echo "#############################################################"
echo "##                    Org3 channel update                  ##"
echo "#############################################################"

setGlobals 3
peer channel update -o orderer.sp.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org3MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem
sleep 4

#
# blockers
#

export CHAINCODE_SOURCE_NAME=1_park_blockers_cc
export CHAINCODE_INSTALL_NAME=park_blockers_cc

echo "##########################################################"
echo "##          Install chaincode blocker on Org1           ##"
echo "##########################################################"

setGlobals 1
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##          Install chaincode blocker on Org2           ##"
echo "##########################################################"

setGlobals 2
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##          Install chaincode blocker on Org3           ##"
echo "##########################################################"

setGlobals 3
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##         Instantiate chaincode blocker on Org1        ##"
echo "##########################################################"

setGlobals 1
peer chaincode instantiate -o orderer.sp.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem -C $CHANNEL_NAME -n $CHAINCODE_INSTALL_NAME -v 1.0 -c '{"Args":["init"]}'
sleep 4

echo "##########################################################"
echo "##              Invoke chaincode blocker                ##"
echo "##########################################################"

peer chaincode invoke -o orderer.sp.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem -C $CHANNEL_NAME -n $CHAINCODE_INSTALL_NAME --peerAddresses peer0.org1.sp.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.sp.com/peers/peer0.org1.sp.com/tls/ca.crt --peerAddresses peer0.org2.sp.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.sp.com/peers/peer0.org2.sp.com/tls/ca.crt --peerAddresses peer0.org3.sp.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.sp.com/peers/peer0.org3.sp.com/tls/ca.crt -c '{"Args":["set", "space0", "{\"SpaceId\": \"space10\"}"]}'
sleep 4

echo "##########################################################"
echo "##               Query chaincode blocker                ##"
echo "##########################################################"

peer chaincode query -n $CHAINCODE_INSTALL_NAME -c '{"Args":["get","space0"]}' -C $CHANNEL_NAME
sleep 4

#
# park
#

export CHAINCODE_SOURCE_NAME=1_park_cc
export CHAINCODE_INSTALL_NAME=park_cc

echo "##########################################################"
echo "##            Install chaincode park on Org1            ##"
echo "##########################################################"

setGlobals 1
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##            Install chaincode park on Org2            ##"
echo "##########################################################"

setGlobals 2
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##            Install chaincode park on Org3            ##"
echo "##########################################################"

setGlobals 3
peer chaincode install -n $CHAINCODE_INSTALL_NAME -v 1.0 -p github.com/chaincode/$CHAINCODE_SOURCE_NAME/go/
sleep 4

echo "##########################################################"
echo "##          Instantiate chaincode park on Org1          ##"
echo "##########################################################"

setGlobals 1
peer chaincode instantiate -o orderer.sp.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem -C $CHANNEL_NAME -n $CHAINCODE_INSTALL_NAME -v 1.0 -c '{"Args":["init"]}'
sleep 4

echo "##########################################################"
echo "##                Invoke chaincode park                 ##"
echo "##########################################################"

peer chaincode invoke -o orderer.sp.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/sp.com/orderers/orderer.sp.com/msp/tlscacerts/tlsca.sp.com-cert.pem -C $CHANNEL_NAME -n $CHAINCODE_INSTALL_NAME --peerAddresses peer0.org1.sp.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.sp.com/peers/peer0.org1.sp.com/tls/ca.crt --peerAddresses peer0.org2.sp.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.sp.com/peers/peer0.org2.sp.com/tls/ca.crt --peerAddresses peer0.org3.sp.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.sp.com/peers/peer0.org3.sp.com/tls/ca.crt -c '{"Args":["set", "space10", "{\"carId\": 100,\"ts\": \"2018-01-01T17:49:50.746191764Z\"}"]}'
sleep 4

echo "##########################################################"
echo "##                Query chaincode park                  ##"
echo "##########################################################"

peer chaincode query -n $CHAINCODE_INSTALL_NAME -c '{"Args":["getBlockingCar","space0"]}' -C $CHANNEL_NAME
