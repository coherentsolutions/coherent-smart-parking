package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"encoding/json"
	"log"
	"strings"
)


type ParkSpaceLoked struct {
	SpaceId string       `json:"SpaceId"`
}

type ParkSpaceLockedWithSpaceId struct {
	SpaceId string  `json:"spaceIdlocked"`
	Space   ParkSpaceLoked `json:"info"`
}

func (t *ParkSpaceLoked) Bytes() []byte {
	var out []byte
	if data, err := json.MarshalIndent(t, "", "  "); err != nil {
		log.Fatal("Unable to marshal report: ", err)
	} else {
		out = data
	}
	return out
}

func (t *ParkSpaceLoked) String() string {
	return string(t.Bytes())
}

// Init is called during chaincode instantiation to initialize any
// data. Note that chaincode upgrade also calls this function to reset
// or to migrate data.
func (t *ParkSpaceLoked) Init(stub shim.ChaincodeStubInterface) peer.Response {

	// Set up any variables or assets here by calling stub.PutState()
	// Store the key and the value on the ledger
	lotIds := []string{"space0", "space1",
					   "space2", "space3",
					   "space4", "space5",
					   "space6", "space7",
					   "space8", "space9",
					   "space10", "space11",
					   "space12", "space13",
					   "space14", "space15",
					   "space16", "space17",
					   "space18", "space19",
					   "space20", "space21",
					   "space22", "space23",
					   "space24", "space25",
					   "space26", "space27",
					   "space28", "space29",
					   "space30", "space31",
					   "space32", "space33",
					   "space34", "space35",
					   "space36"}
	for _, id := range lotIds {
		lot := &ParkSpaceLoked{SpaceId: "0"}
		err := stub.PutState(id, lot.Bytes())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to create asset: %v", id))
		}
	}

	return shim.Success(nil)
}

func (t *ParkSpaceLoked) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	var result string
	var err error
	if fn == "set" {
		result, err = set(stub, args)
	} else if fn == "getAll" {
		result, err = getAll(stub, args)
	} else if fn == "get" {
		result, err = get(stub, args)
	} else { // assume 'get' even if fn is nil
			err = fmt.Errorf("Can't recognize func with name: %v", fn)
		}
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return the result as success payload
	return shim.Success([]byte(result))
}

func set(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 2 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key and a value")
	}
	fmt.Printf("Invoke the function 'set' \n")
	lot := ParkSpaceLoked{}
	json.Unmarshal([]byte(args[1]), &lot)
	value, erro := stub.GetState(args[0])
	if erro != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], erro)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}

	err := stub.PutState(args[0], lot.Bytes())
	if err != nil {
		return "", fmt.Errorf("Failed to set asset: %s", args[0])
	}
	return args[1], nil
}

func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}
	fmt.Printf("Invoke the function 'get' \n")
	value, err := stub.GetState(args[0])
	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}
	
	lot := ParkSpaceLoked{}
	json.Unmarshal(value, &lot)
	return lot.String(), nil
}

// This will execute a key range query on all keys starting with 'space0'
func getAll(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	fmt.Printf("Invoke the function 'getAll' \n")
	startKey := "space0"
	endKey := "space37"

	resultsIterator, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return "", err
	}
	defer resultsIterator.Close()

	var output []ParkSpaceLockedWithSpaceId

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return "", err
		}

		lot := ParkSpaceLoked{}
		json.Unmarshal(queryResponse.Value, &lot)
		output = append(output, ParkSpaceLockedWithSpaceId{queryResponse.Key, lot})
	}

	var stringOut []string
	for _, i := range output {
		bytes, _ := json.Marshal(i)
		stringOut = append(stringOut, string(bytes[:]))
	}

	return strings.Join(stringOut, ","), nil
}

func main() {

	if err := shim.Start(new(ParkSpaceLoked)); err != nil {
		fmt.Printf("Error starting ParkLot chaincode: %s", err)
	}
}
