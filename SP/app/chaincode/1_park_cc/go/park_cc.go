package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"encoding/json"
	"log"
	"time"
	"strings"
)

// ParkSpace implements a simple chaincode to manage an asset
type ParkSpace struct {
	CarId int       `json:"carId"`
	Ts    time.Time `json:"ts"`
}

type ParkSpaceWithSpaceId struct {
	SpaceId string  `json:"spaceId"`
	Space   ParkSpace `json:"info"`
}

type ParkSpaceLoked struct {
	SpaceId string       `json:"SpaceId"`
}

func (t *ParkSpace) Bytes() []byte {
	var out []byte
	if data, err := json.MarshalIndent(t, "", "  "); err != nil {
		log.Fatal("Unable to marshal report: ", err)
	} else {
		out = data
	}
	return out
}

func (t *ParkSpace) String() string {
	return string(t.Bytes())
}


func toChaincodeArgs(args ...string) [][]byte {
	bargs := make([][]byte, len(args))
	for i, arg := range args {
		bargs[i] = []byte(arg)
	}
	return bargs
}

// Init is called during chaincode instantiation to initialize any
// data. Note that chaincode upgrade also calls this function to reset
// or to migrate data.
func (t *ParkSpace) Init(stub shim.ChaincodeStubInterface) peer.Response {

	// Set up any variables or assets here by calling stub.PutState()
	// Store the key and the value on the ledger
	lotIds := []string{"space0", "space1",
					   "space2", "space3",
					   "space4", "space5",
					   "space6", "space7",
					   "space8", "space9",
					   "space10", "space11",
					   "space12", "space13",
					   "space14", "space15",
					   "space16", "space17",
					   "space18", "space19",
					   "space20", "space21",
					   "space22", "space23",
					   "space24", "space25",
					   "space26", "space27",
					   "space28", "space29",
					   "space30", "space31",
					   "space32", "space33",
					   "space34", "space35",
					   "space36"}
	for _, id := range lotIds {
		lot := &ParkSpace{CarId: 0, Ts: time.Now()}
		err := stub.PutState(id, lot.Bytes())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to create asset: %v", id))
		}
	}

	return shim.Success(nil)
}


// Invoke is called per transaction on the chaincode. Each transaction is
// either a 'get' or a 'set' on the asset created by Init function. The Set
// method may create a new asset by specifying a new key-value pair.
func (t *ParkSpace) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	var result string
	var err error
	// Handle different functions
	if fn == "set" {
		result, err = set(stub, args)
	} else if fn == "getAll" {
		result, err = getAll(stub, args)
	} else if fn == "get" {
		result, err = get(stub, args)
	} else if fn == "getBlockingCar" {
		result, err = getBlockingCar(stub, args)
	} else { // assume 'get' even if fn is nil
		err = fmt.Errorf("Can't recognize func with name: %v", fn)
	}
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return the result as success payload
	return shim.Success([]byte(result))
}
//Invoke query another chaincode to obtain a space that blocks the space
func getBlockingCar(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	fmt.Printf("Invoke the function 'getBlockingCar' \n")
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}
	
	chainCodeArgs := toChaincodeArgs("get", args[0])
	response := stub.InvokeChaincode("park_blockers_cc", chainCodeArgs, "")

	if response.Status != shim.OK {
		return "", fmt.Errorf("Failed to query chaincode. Got error: %s", response.Payload)
	}
	lotbl := ParkSpaceLoked{}
	json.Unmarshal([]byte(response.Payload), &lotbl)
	args[0]=lotbl.SpaceId
	value, err := stub.GetState(args[0])
	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}
	
	lot := ParkSpace{}
	json.Unmarshal(value, &lot)
	return lot.String(), nil
}

// Set stores the asset (both key and value) on the ledger. If the key exists,
// it will override the value with the new one
func set(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	fmt.Printf("Invoke the function 'set' \n")
	if len(args) != 2 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key and a value")
	}

	lot := ParkSpace{}
	json.Unmarshal([]byte(args[1]), &lot)
	value, erro := stub.GetState(args[0])
	if erro != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], erro)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}
	currentlot := ParkSpace{}
	json.Unmarshal(value, &currentlot)
	
	if int(lot.CarId)!=int(0){
		if currentlot.CarId!=0 {
			if  currentlot.CarId!=lot.CarId{
				fmt.Printf("Error: This space is already occupied by another car.\n")
				return "", fmt.Errorf("Incorrect. space is occupied")
			}
			 if  currentlot.CarId==lot.CarId{
				 stub.SetEvent("wrarning_car_already_in_place",lot.Bytes())
			     fmt.Printf("Warning: This car is already in this place.\n")
			}	
		}	
	}
	
	// The event "wrong_car_on_vip_place" occurs when the car is parked in an unauthorized place. 
	if args[0] == "space30" {
		if lot.CarId != 136{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes())
			}
		}
	}
	if args[0] == "space31" {
		if lot.CarId != 137{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes()) 
			}
		}
	}
	if args[0] == "space32" {
		if lot.CarId != 138{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes()) 
			}
		}
	}
	if args[0] == "space33" {
		if lot.CarId != 139{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes()) 
			}
		}
	}
	if args[0] == "space34" {
		if lot.CarId != 140{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes()) 
			}
		}
	}
	if args[0] == "space35" {
		if lot.CarId != 141{
			if lot.CarId!=0 {
				stub.SetEvent("wrong_car_on_vip_place",lot.Bytes()) 
			}
		}
	}

	err := stub.PutState(args[0], lot.Bytes())
	if err != nil {
		return "", fmt.Errorf("Failed to set asset: %s", args[0])
	}
	return args[1], nil
}

// Get returns the value of the specified asset key
func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}
	fmt.Printf("Invoke the function 'get' \n")
	value, err := stub.GetState(args[0])
	

	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}
	
	lot := ParkSpace{}
	json.Unmarshal(value, &lot)
	return lot.String(), nil
}

// This will execute a key range query on all keys starting with 'space0'
func getAll(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	fmt.Printf("Invoke the function 'getAll' \n")
	startKey := "space0"
	endKey := "space37"

	resultsIterator, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return "", err
	}
	defer resultsIterator.Close()

	var output []ParkSpaceWithSpaceId

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return "", err
		}

		lot := ParkSpace{}
		json.Unmarshal(queryResponse.Value, &lot)
		output = append(output, ParkSpaceWithSpaceId{queryResponse.Key, lot})
	}

	var stringOut []string
	for _, i := range output {
		bytes, _ := json.Marshal(i)
		stringOut = append(stringOut, string(bytes[:]))
	}

	return strings.Join(stringOut, ","), nil
}

// main function starts up the chaincode in the container during instantiate
func main() {
	if err := shim.Start(new(ParkSpace)); err != nil {
		fmt.Printf("Error starting ParkLot chaincode: %s", err)
	}
}
