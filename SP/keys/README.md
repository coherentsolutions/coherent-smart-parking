## Keys
SSH keys are generated in the first run of the create.sh script.
For using your own SSH keys:

- comment the strings which generate SSH keys in the create.sh script.
- copy your SSH keys to the ./keys folder.
- set it in the terraform.tfvars file.
